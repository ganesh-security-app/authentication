import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';



@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const user = this.authenticationService.currentUserValue;
        if (user) {
            return true;
        }
        let queryParams = null;
        if(state.url && state.url !== "/"){
            queryParams= { returnUrl: state.url }
        }
        this.router.navigate(['/login'], {queryParams});
        return false;
    }
}