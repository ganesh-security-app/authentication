import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { User } from "../models/user";
import { map } from 'rxjs/operators';

@Injectable(
        { providedIn: 'root' }
        )
export class AuthenticationService {
    private userSubject: BehaviorSubject<User>;
    public user: Observable<User>;

    constructor(private http: HttpClient) {
        this.userSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('authentication-app-user')));
        this.user = this.userSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.userSubject.value;
    }

    login(username, password) {
        return this.http.post<any>(`/webapi/user/authenticate`, { username, password })
            .pipe(map(user => {
                localStorage.setItem('authentication-app-user', JSON.stringify(user));
                this.userSubject.next(user);
                return user;
            }));
    }

    logout() {
        localStorage.removeItem('authentication-app-user');
        this.userSubject.next(null);
    }
}