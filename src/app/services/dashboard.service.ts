import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';



@Injectable(
    { providedIn: 'root' }
)
export class DashboardService {
    constructor(private http: HttpClient) { }

    gatData() {
        return this.http.get(`/webapi/securedcontent/`);
    }

    
}