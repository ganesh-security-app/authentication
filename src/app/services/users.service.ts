import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';


@Injectable(
    { providedIn: 'root' }
)
export class UsersService {
    constructor(private http: HttpClient) { }
    signUp(user: User) {
        return this.http.post(`/webapi/user/signup`, user);
    }

    
}