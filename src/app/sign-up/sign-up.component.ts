import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsersService } from '../services/users.service';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {

  signUpForm: FormGroup;
  isLoading = false;
  isSubmitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UsersService,
    private toastr: ToastrService
  ) { }

  ngOnInit() {
    this.init();
  }
  init(){
    this.signUpForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }
  onFormSubmit(){
    this.isSubmitted = true;
    if (this.signUpForm.invalid) {
      return;
    }
    this.isLoading = true;
        this.userService.signUp(this.signUpForm.value)
            .pipe(first())
            .subscribe(
                data => {
                  this.isLoading = false;
                  this.isSubmitted = false;
                  this.init();
                  const template =
                    `Successfully signed up. 
                    <a class="btn btn-primary" href='/login'">Login</a> to continue`;

                  this.toastr.info(template, null, 
                      {positionClass: "toast-top-full-width", enableHtml: true, timeOut: 10 * 1000});
                },
                error => {
                    this.isLoading = false;
                    this.toastr.error("Unable to signup.", null, 
                    {positionClass: "toast-top-full-width"});
                    this.toastr.info("Use Username: user /  Password: password for this demo", null, 
                      {positionClass: "toast-top-full-width"});
                });
  }
}

